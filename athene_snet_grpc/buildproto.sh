python3 -m grpc_tools.protoc \
              -I. \
              --python_out=. \
              --grpc_python_out=. \
              athene_grpc/service_spec/athenefnc.proto

python3 -m grpc_tools.protoc \
              -I. \
              --python_out=. \
              --grpc_python_out=. \
              athene_grpc/service_spec/service_proto.proto

python3 -m grpc_tools.protoc \
              -I. \
              --python_out=. \
              --grpc_python_out=. \
              athene_grpc/service_spec/telemetry.proto