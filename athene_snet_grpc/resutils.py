from resource import *
import json
import time
import requests
import os
import subprocess
import sys
import grpc

import athene_grpc.service_spec.telemetry_pb2_grpc as telemetry_pb2_grpc
import athene_grpc.service_spec.telemetry_pb2 as telemetry_pb2

class resutils():
    def __init__(self):
        self.device_name="athene"


    def memory_usage(self):
        to_MB = 1024.
        to_MB *= to_MB
        return getrusage(RUSAGE_SELF).ru_maxrss / to_MB

    def cpu_ticks(self):
        sec= time.clock()
        tick=sec*(10**7)
        mtick=tick/10**6
        return mtick

    def block_in(self):
        to_KB = 1024.
        return getrusage(RUSAGE_SELF).ru_inblock/to_KB
        
    def get_address(self,service_name):   
        service_description=requests.get("https://consul.icog-labs.com/v1/catalog/service/"+service_name)
        service_description=service_description.content
        service_description=json.loads(service_description.decode('utf8'))[0]
        service_address=service_description["ServiceAddress"]
        service_port=service_description["ServiceTaggedAddresses"]["lan_ipv4"]["Port"]
        return str(service_address),str(service_port)

    def call_telemetry(self,stance_pred,cpu_used,memory_used,net_used,time_taken):
        service_name=os.environ['nunet_adapter_name']
        req_address=self.get_address(service_name)
        address=req_address[0]
        port=req_address[1]
        nunet_adapter_address=address+":"+port
        channel = grpc.insecure_channel("{}".format(nunet_adapter_address))
        stub = telemetry_pb2_grpc.NunetAdapterStub(channel)
        result=stub.telemetry(telemetry_pb2.TelemetryInput(result=stance_pred,cpu_used=cpu_used,memory_used=memory_used,net_used=net_used,time_taken=time_taken,device_name=self.device_name))
        return str(result.response)
