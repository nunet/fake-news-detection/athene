import sys, os.path as path
import json
import os

import requests

import grpc
import time
from concurrent import futures

sys.path.append("./athene_grpc/service_spec")
import athenefnc_pb2_grpc as pb2_grpc
import athenefnc_pb2 as pb2
from resutils import *
import log

from run_athene_service import Log

import service_proto_pb2
import service_proto_pb2_grpc

try:
    grpc_port = os.getenv("NOMAD_PORT_rpc")
except:
    grpc_port = "7008" 

logger=Log.logger

class GRPCserver(pb2_grpc.AtheneStanceClassificationServicer):
    def stance_classify(self, req, ctxt):
        try:
            telemetry=resutils()
            start_time=time.time()
            cpu_start_time=telemetry.cpu_ticks()
        except Exception as e:
            logger.error(e)
        
        athene_system_name=os.environ['athene_system_name']
        req_address=telemetry.get_address(athene_system_name)
        address=req_address[0]
        port=req_address[1]
        athene_system_address="http://"+address+":"+port
        headline = req.headline
        body = req.body
        lbld_pred = json.loads(requests.post(athene_system_address,
                    headers={'Content-Type': 'application/json'},
                    data=json.dumps({'headline' : headline, 'body': body})).text)
        stance_pred = pb2.Stance()
        resp=pb2.Resp()
        stance_pred.agree = float(lbld_pred['agree'])
        stance_pred.disagree = float(lbld_pred['disagree'])
        stance_pred.discuss = float(lbld_pred['discuss'])
        stance_pred.unrelated = float(lbld_pred['unrelated'])
        response=""
        try:
            memory_used=telemetry.memory_usage()
            time_taken=time.time()-start_time
            cpu_used=telemetry.cpu_ticks()-cpu_start_time
            net_used=telemetry.block_in()
            res = {"agree": float(lbld_pred['agree']), 
                "disagree": float(lbld_pred['disagree']),
                "discuss" : float(lbld_pred['discuss']),
                "unrelated" : float(lbld_pred['unrelated'])}
            txn_hash=telemetry.call_telemetry(str(res),cpu_used,memory_used,net_used,time_taken)
            response=[str(res),str(txn_hash)]
            response=str(response)
            logger.info(response)
        except Exception as e:
            logger.error(e)
        resp.response=response
        logger.info(str(resp.response))  
        logger.info(str(stance_pred))  
        return resp

class GRPCproto(service_proto_pb2_grpc.ProtoDefnitionServicer):
    def req_service_price(self, req, ctxt):
        priceParams=service_proto_pb2.priceRespService()
        priceParams.cost_per_process=125
        priceParams.pubk="0x17BA49f80a4Bcb8Ed3c66Ab107bBB40EC3a63370"
        return priceParams

    def req_msg(self, req, ctxt):
        #TODO:  use https://googleapis.dev/python/protobuf/latest/ instead of reading from file 
        with open('athene_grpc/service_spec/athenefnc.proto', 'r') as file:
            proto_str = file.read()
        reqMessage=service_proto_pb2.reqMessage()
        reqMessage.proto_defnition=proto_str
        reqMessage.service_stub="AtheneStanceClassificationStub"
        reqMessage.service_input="InputData"
        reqMessage.function_name="stance_classify"
        return reqMessage

    def req_metadata(self, req, ctxt):
        #TODO:  use https://googleapis.dev/python/protobuf/latest/ instead of reading from file 
        with open('athene_grpc/service_spec/athenefnc.proto', 'r') as file:
            proto_str = file.read()
        service_name=req.service_name
        
        respMetadata=service_proto_pb2.respMetadata()
        
        proto_defnition=proto_str
        service_stub="AtheneStanceClassificationStub"
        service_input="InputData"
        function_name="stance_classify"
        
        if "testing-athene" == req.service_name or "testing-athene-single-adapter" == req.service_name:
            with open('athene_grpc/service_spec/athene.json', 'r') as file:
                service_definition_str = file.read()
        
        if "testing-nlp-model" == req.service_name or "testing-nlp-model-single-adapter" == req.service_name:
            with open('athene_grpc/service_spec/nlp-model.json', 'r') as file:
                service_definition_str = file.read()
        
        if "testing-athene-system" == req.service_name or "testing-athene-system-single-adapter" == req.service_name:
            with open('athene_grpc/service_spec/athene-system.json', 'r') as file:
                service_definition_str = file.read()
        
        if "testing-stanford-nlp-stanza" == req.service_name or "testing-stanford-nlp-stanza-single-adapter" == req.service_name:
            with open('athene_grpc/service_spec/stanford-nlp-stanza.json', 'r') as file:
                service_definition_str = file.read()

        service_definition=json.loads(service_definition_str)
        service_definition["declarations"]["protobuf_definition"]=proto_defnition
        service_definition["declarations"]["service_stub"]=service_stub
        service_definition["declarations"]["function"]=function_name
        service_definition["declarations"]["input"]=service_input

        
        respMetadata.service_definition=json.dumps(service_definition)
        return respMetadata


if __name__ == '__main__':
    grpc_server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    pb2_grpc.add_AtheneStanceClassificationServicer_to_server(GRPCserver(), grpc_server)
    service_proto_pb2_grpc.add_ProtoDefnitionServicer_to_server(GRPCproto(), grpc_server)
    grpc_server.add_insecure_port('[::]:' + grpc_port)
    grpc_server.start()
    logger.info("GRPC Server Started on port: " + grpc_port)
    try:
        while True:
            time.sleep(10)
    except KeyboardInterrupt:
        logger.error("Exiting....")
        grpc_server.stop(0)
